############################################################################
##
## Copyright (C) 2019 The Qt Company Ltd.
## Contact: https://www.qt.io/licensing/
##
## This file is part of the Boot to Qt meta layer.
##
## $QT_BEGIN_LICENSE:GPL$
## Commercial License Usage
## Licensees holding valid commercial Qt licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and The Qt Company. For licensing terms
## and conditions see https://www.qt.io/terms-conditions. For further
## information use the contact form at https://www.qt.io/contact-us.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3 or (at your option) any later version
## approved by the KDE Free Qt Foundation. The licenses are as published by
## the Free Software Foundation and appearing in the file LICENSE.GPL3
## included in the packaging of this file. Please review the following
## information to ensure the GNU General Public License requirements will
## be met: https://www.gnu.org/licenses/gpl-3.0.html.
##
## $QT_END_LICENSE$
##
############################################################################

DEPLOY_CONF_NAME:ebisu      = "Renesas R-Car-E3 Ebisu 4D"
DEPLOY_CONF_NAME:m3ulcb     = "Renesas R-Car-M3 Starter Kit Pro"
DEPLOY_CONF_NAME:h3ulcb     = "Renesas R-Car-H3 Starter Kit Premier"
DEPLOY_CONF_NAME:salvator-x = "Renesas R-Car H3 M3 Salvator-X(S)"

IMAGE_BUILDINFO_VARS:append = " SOC_FAMILY"

IMAGE_FSTYPES += "wic.xz"
DEPLOY_CONF_IMAGE_TYPE = "wic.xz"
WKS_FILE = "sdimage-bootpart.wks"
WIC_CREATE_EXTRA_ARGS = "--no-fstab-update"
WKS_FILE_DEPENDS += "u-boot optee-os"

IMAGE_BOOT_FILES = "\
    Image \
    r*.dtb \
    "

QBSP_IMAGE_CONTENT = "\
    ${IMAGE_LINK_NAME}.${DEPLOY_CONF_IMAGE_TYPE} \
    ${IMAGE_LINK_NAME}.info \
    ${IMAGE_LINK_NAME}.conf \
    bl2-${MACHINE}.srec \
    bl31-${MACHINE}.srec \
    tee-${MACHINE}.srec \
    bootparam_sa0.srec \
    cert_header_sa6.srec \
    u-boot-elf-${MACHINE}.srec \
    "

BOOT_SPACE = "32768"

MACHINE_FEATURES:append = " gsx multimedia"

DISTRO_FEATURES:remove = "ld-is-gold"

BB_MULTI_PROVIDER_ALLOWED += "virtual/libgl virtual/egl virtual/libgles1 virtual/libgles2 virtual/libgbm"

#DISTRO_FEATURES:append = " pam"
PREFERRED_PROVIDER_virtual/libgles1 = ""
PREFERRED_PROVIDER_virtual/libgles2 = "gles-user-module"
PREFERRED_PROVIDER_virtual/egl = "libegl"
PREFERRED_PROVIDER_virtual/libgl = ""
PREFERRED_PROVIDER_virtual/mesa = ""
PREFERRED_PROVIDER_libgbm = "libgbm"
PREFERRED_PROVIDER_libgbm-dev = "libgbm"
PREFERRED_PROVIDER_virtual/libgbm = "libgbm"
BBMASK += "mesa-gl"

# remove vulkan to dismiss depends on mesa and its conflicts with gles-user-module
DISTRO_FEATURES:remove = "vulkan"

# internal copy of R-Car_Series_Evaluation_Software_Packages used for CI
FILESEXTRAPATHS:append = "${BSPDIR}/sources/renesas-rcar3/${PN}:"

# If need cogl, add layer meta-openembedded/meta-gnome
# If need gstreamer, see meta-renesas/meta-rcar-gen3/docs/sample/conf/
BBMASK += "\
    meta-rcar-gen3/recipes-core/packagegroups \
    meta-rcar-gen3/recipes-graphics/cogl/cogl-1.0_1.%.bbappend \
    meta-rcar-gen3/recipes-multimedia/gstreamer \
    "

# From meta-renesas/meta-rcar-gen3/recipes-graphics/images/core-image-weston.inc,
# in particular to get wayland-wsegl.so in the image
IMAGE_INSTALL:append = " \
    packagegroup-wayland-community \
    packagegroup-graphics-renesas-proprietary \
    packagegroup-graphics-renesas-wayland \
    "
MACHINE_FEATURES:append:salvator-x = " usb3"
