# See meta-renesas patch #7320fd6e:
# Revert "rcar-gen3: libgbm: Add PROVIDES for nativesdk-libgbm
PROVIDES:remove = "virtual/nativesdk-libgbm virtual/libgbm-native"
